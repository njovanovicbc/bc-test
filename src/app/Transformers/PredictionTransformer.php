<?php
declare(strict_types=1);

namespace App\Transformers;

use App\Models\Prediction;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="Prediction",
 *     type="object",
 *     description="Prediction Model",
 *     properties={
 *         @OA\Property(property="id", type="integer", example=123),
 *         @OA\Property(property="event_id", type="integer", example=456),
 *         @OA\Property(property="market_type", type="string", example="correct_score or 1x2"),
 *         @OA\Property(property="prediction", type="string", example="X, 1, 2 or correct result like 1:2"),
 *         @OA\Property(property="status", type="string", example="unresolved, win, lost")
 *     }
 * )
 */
class PredictionTransformer extends TransformerAbstract
{
    /**
     * @param Prediction $prediction
     * @return array
     */
    public function transform(Prediction $prediction): array
    {
        return [
            'id' => $prediction->getId(),
            'event_id' => $prediction->getEventId(),
            'market_type' => $prediction->getMarketType(),
            'prediction' => $prediction->getPrediction(),
            'status' => $prediction->getStatus(),
        ];
    }
}
