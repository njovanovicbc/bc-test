<?php
declare(strict_types=1);

namespace App\Transformers;

use App\Models\Token;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *     schema="TokenResponse",
 *     type="object",
 *     description="Token object",
 *     properties={
 *         @OA\Property(property="accessToken", type="string", example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3YxXC9sb2dpbiIsImlhdCI6MTYzMDMzNTU1Niwi"),
 *         @OA\Property(property="token_type", type="string", example="bearer"),
 *         @OA\Property(property="expires_in", type="integer", example=3600)
 *     }
 * )
 */
class TokenTransformer extends TransformerAbstract
{
    /**
     * @param Token $token
     * @return array
     */
    public function transform(Token $token): array
    {
        return [
            'accessToken' => $token->getAccessToken(),
            'token_type' => $token->getType(),
            'expires_in' => $token->getExpiresIn()
        ];
    }
}
