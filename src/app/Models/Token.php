<?php
declare(strict_types=1);

namespace App\Models;

class Token
{
    public const TYPE = 'bearer';

    /** @var string */
    private $accessToken;

    /** @var string */
    private $type;

    /** @var int */
    private $expiresIn;

    public function __construct()
    {
        $this->type = self::TYPE;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     * @return $this
     */
    public function setAccessToken(string $accessToken): Token
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): Token
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getExpiresIn(): int
    {
        return $this->expiresIn;
    }

    /**
     * @param int $expiresIn
     * @return $this
     */
    public function setExpiresIn(int $expiresIn): Token
    {
        $this->expiresIn = $expiresIn;
        return $this;
    }
}
