<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @package App\Models
 * @property int $id
 * @property int $event_id
 * @property string $market_type
 * @property string $prediction
 * @property string $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Prediction extends Model
{
    public const TYPE_3_WAY = '1x2';
    public const TYPE_CORRECT_RESULT = 'correct_score';

    public const STATUS_UNRESOLVED = 'unresolved';
    public const STATUS_WIN = 'win';
    public const STATUS_LOST = 'lost';

    public const VALID_3_WAY_PREDICTIONS = ['1', '2', 'X', 'x'];

    protected $fillable = [
        'event_id',
        'market_type',
        'prediction',
        'status',
    ];

    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->table = 'predictions';

        $this->status = self::STATUS_UNRESOLVED;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getEventId(): int
    {
        return $this->event_id;
    }

    /**
     * @param int $event_id
     * @return $this
     */
    public function setEventId(int $event_id): Prediction
    {
        $this->event_id = $event_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getMarketType(): string
    {
        return $this->market_type;
    }

    /**
     * @param string $market_type
     * @return $this
     */
    public function setMarketType(string $market_type): Prediction
    {
        $this->market_type = $market_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrediction(): string
    {
        return $this->prediction;
    }

    /**
     * @param string $prediction
     * @return $this
     */
    public function setPrediction(string $prediction): Prediction
    {
        $this->prediction = $prediction;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status): Prediction
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return array
     */
    public static function getValidTypes(): array
    {
        return [
            self::TYPE_3_WAY,
            self::TYPE_CORRECT_RESULT
        ];
    }

    /**
     * @return array
     */
    public static function getValidStatuses(): array
    {
        return [
            self::STATUS_UNRESOLVED,
            self::STATUS_WIN,
            self::STATUS_LOST
        ];
    }
}
