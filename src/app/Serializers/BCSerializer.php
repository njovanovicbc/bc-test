<?php
declare(strict_types=1);

namespace App\Serializers;

use League\Fractal\Serializer\ArraySerializer;

/**
 * Class BCSerializer
 * @package App\Serializers
 */
class BCSerializer extends ArraySerializer
{
    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * @param string $resourceKey
     * @param array $data
     * @return array
     */
    public function item($resourceKey, array $data): array
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }

        return $data;
    }

    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * @param string $resourceKey
     * @param array $data
     * @return array
     */
    public function collection($resourceKey, array $data): array
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }

        return $data;
    }

    public function null()
    {
        /** @phpstan-ignore-next-line */
        return null;
    }
}
