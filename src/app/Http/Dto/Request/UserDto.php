<?php
declare(strict_types=1);

namespace App\Http\Dto\Request;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class UserDto extends DataTransferObject
{
    /** @var string */
    public $name;

    /** @var string */
    public $email;

    /** @var string */
    public $password;

    public static function createFromRequest(Request $request): self
    {
        return new self([
            'name' => (string)$request->get('name'),
            'email' => (string)$request->get('email'),
            'password' => (string)$request->get('password')
        ]);
    }
}
