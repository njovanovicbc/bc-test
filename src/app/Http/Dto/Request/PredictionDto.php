<?php
declare(strict_types=1);

namespace App\Http\Dto\Request;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class PredictionDto extends DataTransferObject
{
    /** @var int */
    public $event_id;

    /** @var string */
    public $market_type;

    /** @var string */
    public $prediction;

    public static function createFromRequest(Request $request): self
    {
        return new self([
            'event_id' => (int)$request->get('event_id'),
            'market_type' => strtolower((string)$request->get('market_type')),
            'prediction' => trim((string)$request->get('prediction'))
        ]);
    }
}
