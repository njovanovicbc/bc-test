<?php
declare(strict_types=1);

namespace App\Http\Dto\Request;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

class LoginDto extends DataTransferObject
{
    /** @var string */
    public $email;

    /** @var string */
    public $password;

    public static function createFromRequest(Request $request): self
    {
        return new self([
            'email' => (string)$request->get('email'),
            'password' => (string)$request->get('password')
        ]);
    }
}
