<?php
declare(strict_types=1);

namespace App\Http\Validators;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthValidator extends BaseValidator
{
    /**
     * @param Request $request
     * @throws ValidationException
     */
    public function validateLoginData(Request $request): void
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
    }
}
