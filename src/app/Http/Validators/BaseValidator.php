<?php
declare(strict_types=1);

namespace App\Http\Validators;

use Laravel\Lumen\Routing\ProvidesConvenienceMethods;

class BaseValidator
{
    use ProvidesConvenienceMethods;
}
