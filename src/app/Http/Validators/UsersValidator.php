<?php
declare(strict_types=1);

namespace App\Http\Validators;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class UsersValidator extends BaseValidator
{
    /**
     * @param Request $request
     * @throws ValidationException
     */
    public function validateCreate(Request $request): void
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);
    }
}
