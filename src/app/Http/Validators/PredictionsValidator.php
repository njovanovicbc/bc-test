<?php
declare(strict_types=1);

namespace App\Http\Validators;

use App\Exceptions\InvalidParameterValueException;
use App\Http\Dto\Request\PredictionDto;
use App\Models\Prediction;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PredictionsValidator extends BaseValidator
{
    /**
     * @param Request $request
     * @throws ValidationException
     */
    public function validateDataForCreate(Request $request): void
    {
        $this->validate($request, [
            'event_id' => 'required|integer',
            'market_type' => 'required|string',
            'prediction' => 'required|string',
        ]);
    }

    /**
     * @param PredictionDto $predictionDto
     * @throws InvalidParameterValueException
     */
    public function validateCorrectValuesAreSet(PredictionDto $predictionDto): void
    {
        if (!in_array($predictionDto->market_type, Prediction::getValidTypes())) {
            throw new InvalidParameterValueException('INVALID_MARKET_TYPE, Must be one of: ' . implode(', ', Prediction::getValidTypes()));
        }

        if ($predictionDto->market_type === Prediction::TYPE_3_WAY) {
            $this->validate3WayPrediction($predictionDto->prediction);
            $predictionDto->prediction = strtoupper($predictionDto->prediction);
        } else {
            $this->validateCorrectScorePrediction($predictionDto->prediction);
        }
    }

    /**
     * @param Request $request
     * @return string
     * @throws InvalidParameterValueException
     * @throws ValidationException
     */
    public function validatePredictionStatus(Request $request): string
    {
        $this->validate($request, ['status' => 'required|string']);

        $status = strtolower($request->get('status'));

        if (!in_array($status, Prediction::getValidStatuses())) {
            throw new InvalidParameterValueException(
                'Invalid status, must be one of: '. implode(', ', Prediction::getValidStatuses())
            );
        }

        return $status;
    }

    /**
     * @param string $prediction
     * @throws InvalidParameterValueException
     */
    private function validate3WayPrediction(string $prediction): void
    {
        if (!in_array($prediction, Prediction::VALID_3_WAY_PREDICTIONS)) {
            throw new InvalidParameterValueException(
                'Prediction must be one of: ' . implode(', ', Prediction::VALID_3_WAY_PREDICTIONS)
            );
        }
    }

    /**
     * @param string $prediction
     * @throws InvalidParameterValueException
     */
    private function validateCorrectScorePrediction(string $prediction): void
    {
        if (!preg_match('/\b(0|[1-9]\d*):(0|[1-9]\d*)\b/', $prediction)) {
            throw new InvalidParameterValueException(
                'Invalid prediction format, must be score divided by :, for example 0:0, 1:0, 5:3'
            );
        }
    }
}
