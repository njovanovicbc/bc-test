<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LogDataMiddleware
 * @package App\Http\Middleware
 */
class LogDataMiddleware
{
    private const CONTEXT_API = 'API';
    private const API_VERSION = 'v1';

    /** @var LoggerInterface */
    private $logger;

    /**
     * LogDataMiddleware constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        $path = $request->path();
        $context = $this->getContext();
        $logData = $this->generateInLogData($request, $path);
        $this->logger->info($context, $logData);
        $response = $next($request);
        $logData = $this->generateOutLogData($request, $response, $logData);
        $this->logger->info($context, $logData);

        return $response;
    }

    /**
     * @return string
     */
    private function getContext(): string
    {
        return self::CONTEXT_API;
    }

    /**
     * @param Request $request
     * @param string $path
     * @return array
     */
    private function generateInLogData(Request $request, string $path): array
    {
        $logData = array_merge(
            [
                'direction' => 'in',
                'api_version' => self::API_VERSION
            ],
            $this->getRouteData((array)$request->route(), $request->getMethod(), $path)
        );
        if ($request->server('REQUEST_URI')) {
            $logData['request'] = $request->server('REQUEST_METHOD') . ' ' . $request->server('REQUEST_URI')
                . ' ' . $request->server('SERVER_PROTOCOL');
        }

        return $logData;
    }

    /**
     * @param array $route
     * @param string $httpMethod
     * @param string $routePattern
     * @return array
     */
    private function getRouteData(array $route, string $httpMethod, string $routePattern): array
    {
        $action = '';
        $controller = '';
        $uses =& $route[1]['uses'];
        if (is_string($uses)) {
            [$controller, $action] = explode('@', $uses);
        }
        $pattern =& $route[1]['route_pattern'];
        if (null !== $pattern) {
            $routePattern = $pattern;
        }

        return [
            'class' => $controller,
            'method' => $action,
            'route_pattern' => $httpMethod . ' /' . ltrim($routePattern, '/'),
        ];
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $existingDataToLog
     * @return array
     */
    private function generateOutLogData(Request $request, Response $response,
                                        array $existingDataToLog): array
    {
        $existingDataToLog = array_merge($existingDataToLog, $this->getOutLogData($request));
        $existingDataToLog['direction'] = 'out';
        $existingDataToLog['server_time'] = $this->getServerTime($request);
        if (config('app.log_peak_memory')) {
            $existingDataToLog['peak_mem'] = memory_get_peak_usage(true);
        }
        $existingDataToLog['response_status_code'] = $response->getStatusCode();

        return $existingDataToLog;
    }

    /**
     * @param Request $request
     * @return array
     */
    private function getOutLogData(Request $request): array
    {
        $logData = [];
        try {
            /** @var User|null $authUser */
            $authUser = $request->user();
        } catch (Exception $exception) {
            $authUser = null;
        }
        if ($authUser) {
            $logData['user_id'] = $authUser->getId();
        }

        return $logData;
    }

    /**
     * @param Request $request
     * @return float
     */
    private function getServerTime(Request $request): float
    {
        $serverTime = 0.0;
        $endTime = (float)$request->server('REQUEST_TIME_FLOAT');
        if (null !== $endTime) {
            $serverTime = round(microtime(true) - $endTime, 6);
        }

        return $serverTime;
    }
}
