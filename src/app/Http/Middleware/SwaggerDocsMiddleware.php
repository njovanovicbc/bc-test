<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use App\Exceptions\NotFoundException;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SwaggerDocsMiddleware
 * @package App\Http\Middleware
 */
class SwaggerDocsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return Response
     * @throws NotFoundException
     */
    public function handle($request, Closure $next): Response
    {
        if (!env('ENABLE_SWAGGER')) {
            throw new NotFoundException();
        }

        return $next($request);
    }
}
