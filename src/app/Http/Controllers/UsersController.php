<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Dto\Request\UserDto;
use App\Http\Validators\UsersValidator;
use App\Serializers\BCSerializer;
use App\Services\UserService;
use App\Transformers\UserTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Validation\ValidationException;

class UsersController extends Controller
{
    /** @var UserService */
    private $userService;

    /** @var UsersValidator */
    private $usersValidator;

    /**
     * @param UserService $userService
     * @param UsersValidator $usersValidator
     */
    public function __construct(UserService $userService, UsersValidator $usersValidator)
    {
        $this->userService = $userService;
        $this->usersValidator = $usersValidator;
    }

    /**
     * @OA\Post(
     *     path="/users",
     *     description="Create user",
     *     summary="Create user",
     *     tags={"Users"},
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              type="object",
     *              required={"name", "email", "password", "password_confirmation"},
     *              @OA\Property(
     *                  property="name",
     *                  description="User name",
     *                  type="string",
     *                  example="John Doe"
     *              ),
     *              @OA\Property(
     *                  property="email",
     *                  description="User email",
     *                  type="string",
     *                  example="john_doe@example.com"
     *              ),
     *              @OA\Property(
     *                  property="password",
     *                  description="User password",
     *                  type="string",
     *                  example="doe_john"
     *              ),
     *              @OA\Property(
     *                  property="password_confirmation",
     *                  description="User password confirmed",
     *                  type="string",
     *                  example="doe_john"
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="",
     *         @OA\JsonContent(type="object",
     *              properties={
     *                  @OA\Property(property="prediction", type="object", ref="#/components/schemas/User")
     *              }
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="The email has already been taken. or similar message",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorResponse")
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function create(Request $request): JsonResponse
    {
        $this->usersValidator->validateCreate($request);

        $user = $this->userService->createUser(UserDto::createFromRequest($request));

        $response = fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new BCSerializer())
            ->toArray();

        return response()->json(['user' => $response], Response::HTTP_CREATED);
    }
}
