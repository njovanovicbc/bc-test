<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\InvalidParameterValueException;
use App\Exceptions\NotFoundException;
use App\Http\Dto\Request\PredictionDto;
use App\Http\Validators\PredictionsValidator;
use App\Serializers\BCSerializer;
use App\Services\PredictionService;
use App\Transformers\PredictionTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class PredictionsController extends Controller
{
    /** @var PredictionService */
    private $predictionService;

    /** @var PredictionsValidator */
    private $predictionsValidator;

    /**
     * @param PredictionService $predictionService
     * @param PredictionsValidator $predictionsValidator
     */
    public function __construct(PredictionService $predictionService, PredictionsValidator $predictionsValidator)
    {
        $this->predictionService = $predictionService;
        $this->predictionsValidator = $predictionsValidator;
    }

    /**
     * @OA\Get(
     *     path="/predictions",
     *     description="Returns list of all predictions without pagination",
     *     summary="Returns list of all predictions without pagination",
     *     tags={"Predictions"},
     *     security={{"BearerAuth": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Prediction list",
     *         @OA\JsonContent(
     *              type="object",
     *              properties={
     *                  @OA\Property(property="predictions", type="array", @OA\Items(ref="#/components/schemas/Prediction"))
     *              }
     *         )
     *     )
     * )
     *
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $predictions = $this->predictionService->getAllPredictions();

        $response = fractal()
            ->collection($predictions, new PredictionTransformer())
            ->serializeWith(new BCSerializer())
            ->toArray();

        return response()->json(['predictions' => $response]);
    }

    /**
     * @OA\Post(
     *     path="/predictions",
     *     description="Create prediction",
     *     summary="Create prediction",
     *     tags={"Predictions"},
     *     security={{"BearerAuth": {}}},
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              type="object",
     *              required={"event_id", "market_type", "prediction"},
     *              @OA\Property(
     *                  property="event_id",
     *                  description="Event id",
     *                  type="integer",
     *                  example=987
     *              ),
     *              @OA\Property(
     *                  property="market_type",
     *                  description="Type of the prediction, can be one of the 'correct_score' or '1x2'",
     *                  type="string",
     *                  enum={"correct_score", "1x2"}
     *              ),
     *              @OA\Property(
     *                  property="prediction",
     *                  description="For correct_score type use '1' 'X' or '2', and for the corret_score exact score separated by ':'",
     *                  type="string",
     *                  example="2:0"
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="",
     *         @OA\JsonContent(type="object",
     *              properties={
     *                  @OA\Property(property="prediction", type="object", ref="#/components/schemas/Prediction")
     *              }
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid prediction format, must be score divided by :, for example 0:0, 1:0, 5:3",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorResponse")
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws InvalidParameterValueException
     */
    public function create(Request $request): JsonResponse
    {
        $this->predictionsValidator->validateDataForCreate($request);

        $predictionDto = PredictionDto::createFromRequest($request);

        $this->predictionsValidator->validateCorrectValuesAreSet($predictionDto);

        $prediction = $this->predictionService->createPrediction($predictionDto);

        $response = fractal()
            ->item($prediction, new PredictionTransformer())
            ->serializeWith(new BCSerializer())
            ->toArray();

        return response()->json(['prediction' => $response], Response::HTTP_CREATED);
    }

    /**
     * @OA\Put(
     *     path="/predictions/{predictionId}/status",
     *     description="Update prediction status",
     *     summary="Update prediction status",
     *     tags={"Predictions"},
     *     security={{"BearerAuth": {}}},
     *     @OA\Parameter(
     *         name="predictionId",
     *         in="path",
     *         description="ID of the prediction",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              type="object",
     *              required={"status"},
     *              @OA\Property(
     *                  property="status",
     *                  description="Status of the prediction, can be one of the 'win', 'lost' or 'resolved'",
     *                  type="string",
     *                  enum={"win", "lost", "resolved"}
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\JsonContent(type="object",
     *              properties={
     *                  @OA\Property(property="prediction", type="object", ref="#/components/schemas/Prediction")
     *              }
     *        )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid status, must be one of: unresolved, win, lost",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorResponse")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="PREDICTION_NOT_FOUND",
     *         @OA\JsonContent(ref="#/components/schemas/ErrorResponse")
     *     )
     * )
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws InvalidParameterValueException
     * @throws ValidationException
     * @throws NotFoundException
     */
    public function updateStatus(Request $request, int $id): JsonResponse
    {
        $status = $this->predictionsValidator->validatePredictionStatus($request);

        $prediction = $this->predictionService->updatePredictionStatus($id, $status);

        $response = fractal()
            ->item($prediction, new PredictionTransformer())
            ->serializeWith(new BCSerializer())
            ->toArray();

        return response()->json(['prediction' => $response]);
    }
}
