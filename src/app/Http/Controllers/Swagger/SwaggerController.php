<?php

namespace App\Http\Controllers\Swagger;

use Laravel\Lumen\Routing\UrlGenerator;
use SwaggerLume\Exceptions\SwaggerLumeException;
use SwaggerLume\Generator;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class SwaggerController extends BaseController
{
    /** @var string */
    private $rootUrl;

    public function __construct()
    {
        $this->rootUrl = rtrim(env('APP_URL'), '/');

        /** @var UrlGenerator $generator */
        $generator = App('url');
        $generator->forceRootUrl($this->rootUrl);
    }

    /**
     * @param mixed $asset
     * @return Response
     * @throws SwaggerLumeException
     */
    public function getAsset($asset): Response
    {
        $path = swagger_ui_dist_path($asset);

        return (new Response(
            file_get_contents($path), 200, [
                'Content-Type' => pathinfo($asset)['extension'] == 'css' ?
                    'text/css' : 'application/javascript',
            ]
        ))->setSharedMaxAge(31536000)
            ->setMaxAge(31536000)
            ->setExpires(new \DateTime('+1 year'));
    }
    /**
     * Dump api-docs.json content endpoint.
     *
     * @param null $jsonFile
     *
     * @return Response
     */
    public function docs($jsonFile = null): Response
    {
        $filePath = config('swagger-lume.paths.docs').'/'.
            (! is_null($jsonFile) ? $jsonFile : config('swagger-lume.paths.docs_json'));

        if (! File::exists($filePath)) {
            abort(404, 'Cannot find '.$filePath);
        }

        $content = File::get($filePath);

        return new Response($content, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * Display Swagger API page.
     *
     * @return Response
     */
    public function api(): Response
    {
        if (config('swagger-lume.generate_always')) {
            Generator::generateDocs();
        }

        return new Response(
            view('swagger-lume::index', [
                'secure' => Request::secure(),
                'urlToDocs' => $this->rootUrl . '/api-docs',
                'operationsSorter' => config('swagger-lume.operations_sort'),
                'configUrl' => config('swagger-lume.additional_config_url'),
                'validatorUrl' => config('swagger-lume.validator_url'),
                'oauth2RedirectUrl' => $this->rootUrl . '/api/oauth2-callback',
                'rootUrl' => $this->rootUrl
            ]),
            200,
            ['Content-Type' => 'text/html']
        );
    }

    /**
     * Display Oauth2 callback pages.
     *
     * @return string
     * @throws SwaggerLumeException
     */
    public function oauth2Callback(): string
    {
        /** @phpstan-ignore-next-line */
        return File::get(swagger_ui_dist_path('oauth2-redirect.html'));
    }
}
