<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 *  @OA\OpenApi(
 *     @OA\Info(
 *         version="1.0.0",
 *         title="Nikola BC test API v1.0 Docs",
 *     ),
 *     @OA\Server(
 *          url="http://localhost:85/v1",
 *          @OA\ServerVariable(
 *              serverVariable="protocol",
 *              enum={"http", "https"},
 *              default="http"
 *          ),
 *          @OA\ServerVariable(
 *              serverVariable="port",
 *              enum={"80", "85"},
 *              default="85"
 *          ),
 *          @OA\ServerVariable(
 *              serverVariable="domain",
 *              enum={
 *                  "localhost"
 *              },
 *              default="localhost"
 *          )
 *     )
 * )
 */
class Controller extends BaseController
{
    //
}
