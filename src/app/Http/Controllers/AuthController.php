<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\InvalidPasswordException;
use App\Http\Dto\Request\LoginDto;
use App\Http\Validators\AuthValidator;
use App\Serializers\BCSerializer;
use App\Services\AuthService;
use App\Transformers\TokenTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /** @var AuthService */
    private $authService;

    /** @var AuthValidator */
    private $authValidator;

    /**
     * @param AuthService $authService
     * @param AuthValidator $authValidator
     */
    public function __construct(AuthService $authService, AuthValidator $authValidator)
    {
        $this->authService = $authService;
        $this->authValidator = $authValidator;
    }

    /**
     * @OA\Post(
     *     path="/login",
     *     tags={"Token"},
     *     summary="Get new access token",
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  required={"email", "password"},
     *                  @OA\Property(
     *                      property="email",
     *                      type="string",
     *                      example="john_doe@example.com"
     *                  ),
     *                  @OA\Property(
     *                      property="password",
     *                      type="string",
     *                      example="doe_john"
     *                  )
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Tokens are successfully created.",
     *          @OA\JsonContent(
     *              type="object",
     *              properties={
     *                  @OA\Property(
     *                      property="token",
     *                      ref="#/components/schemas/TokenResponse"
     *                  )
     *              }
     *          )
     *     ),
     *     @OA\Response(
     *          response="400",
     *          description="Error while getting tokens.",
     *          @OA\JsonContent(ref="#/components/schemas/ErrorResponse")
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws InvalidPasswordException
     * @throws ValidationException
     */
    public function login(Request $request): JsonResponse
    {
        $this->authValidator->validateLoginData($request);

        $token = $this->authService->generateAccessToken(LoginDto::createFromRequest($request));

        $response = fractal()
            ->item($token, new TokenTransformer())
            ->serializeWith(new BCSerializer())
            ->toArray();

        return response()->json(['token' => $response]);
    }
}
