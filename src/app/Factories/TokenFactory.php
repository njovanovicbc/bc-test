<?php
declare(strict_types=1);

namespace App\Factories;

use App\Models\Token;

class TokenFactory
{
    /**
     * @param string $accessToken
     * @param int $expiresIn
     * @return Token
     */
    public function make(string $accessToken, int $expiresIn): Token
    {
        return (new Token())
            ->setAccessToken($accessToken)
            ->setExpiresIn($expiresIn);
    }
}
