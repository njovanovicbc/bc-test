<?php
declare(strict_types=1);

namespace App\Factories;

use App\Http\Dto\Request\UserDto;
use App\Models\User;

class UserFactory
{
    /**
     * @param UserDto $userDto
     * @param string $password
     * @return User
     */
    public function makeFromDto(UserDto $userDto, string $password): User
    {
        return (new User())
            ->setEmail($userDto->email)
            ->setName($userDto->name)
            ->setPassword($password);
    }
}
