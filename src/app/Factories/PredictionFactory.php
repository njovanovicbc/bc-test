<?php
declare(strict_types=1);

namespace App\Factories;

use App\Http\Dto\Request\PredictionDto;
use App\Models\Prediction;

class PredictionFactory
{
    /**
     * @param PredictionDto $predictionDto
     * @return Prediction
     */
    public function makeFromDto(PredictionDto $predictionDto): Prediction
    {
        return (new Prediction())
            ->setEventId($predictionDto->event_id)
            ->setMarketType($predictionDto->market_type)
            ->setPrediction($predictionDto->prediction);
    }
}
