<?php
declare(strict_types=1);

namespace App\Services;

use App\Factories\UserFactory;
use App\Http\Dto\Request\UserDto;
use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Contracts\Hashing\Hasher;

class UserService
{
    /** @var UserRepositoryInterface */
    private $userRepository;

    /** @var UserFactory */
    private $userFactory;

    /** @var Hasher */
    private $hasher;

    /**
     * @param UserRepositoryInterface $userRepository
     * @param UserFactory $userFactory
     * @param Hasher $hasher
     */
    public function __construct(UserRepositoryInterface $userRepository, UserFactory $userFactory, Hasher $hasher)
    {
        $this->userRepository = $userRepository;
        $this->userFactory = $userFactory;
        $this->hasher = $hasher;
    }

    /**
     * @param UserDto $userDto
     * @return User
     */
    public function createUser(UserDto $userDto): User
    {
        $password = $this->hasher->make($userDto->password);

        $user = $this->userFactory->makeFromDto($userDto, $password);

        $this->userRepository->saveUser($user);

        return $user;
    }
}
