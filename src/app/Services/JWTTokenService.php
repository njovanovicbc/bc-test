<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\User;
use App\Services\Interfaces\TokenInterface;
use Tymon\JWTAuth\Factory;
use Tymon\JWTAuth\JWT;

class JWTTokenService implements TokenInterface
{
    /** @var JWT */
    private $JWT;

    /** @var Factory */
    private $jwtFactory;

    /**
     * @param JWT $JWT
     * @param Factory $jwtFactory
     */
    public function __construct(JWT $JWT, Factory $jwtFactory)
    {
        $this->JWT = $JWT;
        $this->jwtFactory = $jwtFactory;
    }

    /**
     * @param User $user
     * @return string
     */
    public function getByUser(User $user): string
    {
        return $this->JWT->fromUser($user);
    }

    public function getTTL(): int
    {
        return $this->jwtFactory->getTTL() * 60;
    }
}
