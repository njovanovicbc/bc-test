<?php
declare(strict_types=1);

namespace App\Services\Interfaces;

use App\Models\User;

interface TokenInterface
{
    /**
     * @param User $user
     * @return string
     */
    public function getByUser(User $user): string;

    /**
     * @return int
     */
    public function getTTL(): int;
}
