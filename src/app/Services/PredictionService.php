<?php
declare(strict_types=1);

namespace App\Services;

use App\Exceptions\NotFoundException;
use App\Factories\PredictionFactory;
use App\Http\Dto\Request\PredictionDto;
use App\Models\Prediction;
use App\Repositories\Interfaces\PredictionRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class PredictionService
{
    /** @var PredictionRepositoryInterface */
    private $predictionRepository;

    /** @var PredictionFactory */
    private $predictionFactory;


    public function __construct(PredictionRepositoryInterface $predictionRepository, PredictionFactory $predictionFactory)
    {
        $this->predictionRepository = $predictionRepository;
        $this->predictionFactory = $predictionFactory;
    }

    /**
     * @return Collection
     */
    public function getAllPredictions(): Collection
    {
        return $this->predictionRepository->getAllPredictions();
    }

    /**
     * @param PredictionDto $predictionDto
     * @return Prediction
     */
    public function createPrediction(PredictionDto $predictionDto): Prediction
    {
        $prediction = $this->predictionFactory->makeFromDto($predictionDto);

        $this->predictionRepository->savePrediction($prediction);

        return $prediction;
    }

    /**
     * @param int $id
     * @param string $status
     * @return Prediction
     * @throws NotFoundException
     */
    public function updatePredictionStatus(int $id, string $status): Prediction
    {
        $prediction = $this->predictionRepository->getPrediction($id);

        if (!$prediction) {
            throw new NotFoundException('PREDICTION_NOT_FOUND');
        }

        $prediction->setStatus($status);

        $this->predictionRepository->savePrediction($prediction);

        return $prediction;
    }
}
