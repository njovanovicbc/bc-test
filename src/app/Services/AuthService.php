<?php
declare(strict_types=1);

namespace App\Services;

use App\Exceptions\InvalidPasswordException;
use App\Factories\TokenFactory;
use App\Http\Dto\Request\LoginDto;
use App\Models\Token;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\Interfaces\TokenInterface;
use Illuminate\Contracts\Hashing\Hasher;

class AuthService
{
    /** @var UserRepositoryInterface */
    private $userInterface;

    /** @var TokenInterface */
    private $tokenInterface;

    /** @var TokenFactory */
    private $tokenFactory;

    /** @var Hasher */
    private $hasher;

    /**
     * @param UserRepositoryInterface $userInterface
     * @param TokenInterface $tokenInterface
     * @param TokenFactory $tokenFactory
     * @param Hasher $hasher
     */
    public function __construct(UserRepositoryInterface $userInterface, TokenInterface $tokenInterface,
                                TokenFactory $tokenFactory, Hasher $hasher)
    {
        $this->userInterface = $userInterface;
        $this->tokenInterface = $tokenInterface;
        $this->tokenFactory = $tokenFactory;
        $this->hasher = $hasher;
    }

    /**
     * @param LoginDto $loginDto
     * @return Token
     * @throws InvalidPasswordException
     */
    public function generateAccessToken(LoginDto $loginDto): Token
    {
        $user = $this->userInterface->getByEmail($loginDto->email);

        if ($user && $this->hasher->check($loginDto->password, $user->getPassword())) {
            $accessToken = $this->tokenInterface->getByUser($user);
            $expiresIn = $this->tokenInterface->getTTL();

            return $this->tokenFactory->make($accessToken, $expiresIn);
        }

        throw new InvalidPasswordException();
    }
}
