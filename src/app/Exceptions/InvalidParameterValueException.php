<?php
declare(strict_types=1);

namespace App\Exceptions;

class InvalidParameterValueException extends ApiException
{
    /**
     * @param string|null $message
     */
    public function __construct(string $message = null)
    {
        $message = $message ?? 'INVALID_PARAMETER_VALUE';

        parent::__construct($message);
    }
}
