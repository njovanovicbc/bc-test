<?php
declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class InvalidPasswordException
 * @package App\Exceptions\Employee
 */
class InvalidPasswordException extends ApiException
{
    /**
     * InvalidPasswordException constructor.
     */
    public function __construct()
    {
        parent::__construct('INVALID_USERNAME_OR_PASSWORD');

        $this->responseCode = Response::HTTP_UNAUTHORIZED;
    }
}
