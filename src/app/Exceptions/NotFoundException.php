<?php
declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class NotFoundException
 * @package App\Exceptions
 */
class NotFoundException extends ApiException
{
    /**
     * @param string|null $message
     */
    public function __construct(?string $message = null)
    {
        $message = $message ?? 'HTTP_NOT_FOUND';

        parent::__construct($message);

        $this->responseCode = Response::HTTP_NOT_FOUND;
    }
}
