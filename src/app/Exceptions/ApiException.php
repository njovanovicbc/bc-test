<?php
declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiException
 * @package App\Exceptions
 */
abstract class ApiException extends Exception
{
    /** @var int */
    protected $responseCode;

    /** @var string */
    protected $messageCode;

    /** @var array */
    protected $headers;

    /**
     * ApiException constructor.
     * @param string $message
     */
    public function __construct(string $message = '')
    {
        $this->responseCode = Response::HTTP_BAD_REQUEST;
        $this->messageCode = $message;
        $this->headers = [];
        parent::__construct($message);
    }

    /**
     * @return int
     */
    public function getResponseCode(): int
    {
        return $this->responseCode;
    }

    /**
     * @return string
     */
    public function getMessageCode(): string
    {
        return $this->messageCode;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @noinspection PhpUnusedParameterInspection
     */
    public function render(Request $request): JsonResponse
    {
        $code = $this->getResponseCode();
        $data = [
            'err' => [
                'code' => $code,
                'msg' => $this->getMessage(),
            ],
        ];

        return response()->json($data, $code, $this->getHeaders());
    }
}
