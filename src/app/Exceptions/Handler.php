<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Log\Logger;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

/**
 * @OA\Schema(
 *     schema="ErrorResponse",
 *     type="object",
 *     @OA\Property(
 *          property="err",
 *          type="object",
 *          @OA\Property(
 *              property="code",
 *              type="integer",
 *              example=400
 *          ),
 *          @OA\Property(
 *              property="msg",
 *              type="string",
 *              example="SOME_ERROR_MESSAGE"
 *          )
 *     )
 * )
 */
class Handler extends ExceptionHandler
{
    /** @var Logger */
    private $logger;

    /**s
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Throwable $e
     * @return void
     *
     * @throws Exception
     */
    public function report(Throwable $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request  $request
     * @param Throwable $e
     * @return \Illuminate\Http\Response|JsonResponse
     *
     * @throws Throwable
     */
    public function render($request, Throwable $e)
    {
        if (method_exists($e, 'render')) {
            return $e->render($request);
        }

        $responseCode = Response::HTTP_BAD_REQUEST;
        $message = $e->getMessage();

        if ($e instanceof ValidationException) {
            /** @var ValidationException $responseCode */
            $errors = $e->errors();
            $errorsForAttribute = reset($errors);
            $message = reset($errorsForAttribute);
        }

        $data = [
            'err' => [
                'code' => $responseCode,
                'msg' => $message,
            ],
        ];
        $trace = $e->getTraceAsString();
        $errorContext = ['trace' => $trace];

        if (empty($message)) {
            $message = 'exception class: ' . get_class($e) . ' (exception code: ' . $e->getCode() . ')';
        }
        $this->logger->error($message, $errorContext);

        return response()->json($data, $responseCode);
    }
}
