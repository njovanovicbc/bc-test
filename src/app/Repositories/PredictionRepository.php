<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Models\Prediction;
use App\Repositories\Interfaces\PredictionRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class PredictionRepository implements PredictionRepositoryInterface
{
    /**
     * @return Prediction[]|Collection
     */
    public function getAllPredictions(): Collection
    {
        return Prediction::all();
    }

    /**
     * @param int $id
     * @return Prediction|null
     */
    public function getPrediction(int $id): ?Prediction
    {
        return Prediction::where('id', '=', $id)->first();
    }

    /**
     * @param Prediction $prediction
     */
    public function savePrediction(Prediction $prediction): void
    {
        $prediction->save();
    }
}
