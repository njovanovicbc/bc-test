<?php
declare(strict_types=1);

namespace App\Repositories\Interfaces;

use App\Models\User;

interface UserRepositoryInterface
{
    /**
     * @param User $user
     * @return mixed
     */
    public function saveUser(User $user);

    /**
     * @param string $email
     * @return User|null
     */
    public function getByEmail(string $email): ?User;
}
