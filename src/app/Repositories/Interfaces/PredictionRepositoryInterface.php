<?php
declare(strict_types=1);

namespace App\Repositories\Interfaces;

use App\Models\Prediction;
use Illuminate\Database\Eloquent\Collection;

interface PredictionRepositoryInterface
{
    /**
     * @return Prediction[]|Collection
     */
    public function getAllPredictions(): Collection;

    /**
     * @param int $id
     * @return Prediction|null
     */
    public function getPrediction(int $id): ?Prediction;

    /**
     * @param Prediction $prediction
     */
    public function savePrediction(Prediction $prediction): void;
}
