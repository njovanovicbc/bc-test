<?php
declare(strict_types=1);

namespace App\Providers;

use Illuminate\Log\Logger;
use Illuminate\Log\LogManager;
use Illuminate\Support\ServiceProvider;
use Monolog\Logger as MonologLogger;

/**
 * Class LogServiceProvider
 * @package App\Providers
 */
class LogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $monolog = $this->getMonolog();
        $monolog->pushProcessor(static function ($record) {
            $record['context']['request_uid'] = $_SERVER['HTTP_X_REQUEST_UID'] ?? '';
            if (is_array($record['message']) || is_object($record['message'])) {
                $name = (is_array($record['message'])
                    ? "Array (" . count($record['message']) . ")"
                    : get_class($record['message']));
                $record['context'][$name] = $record['message'];
                $record['message'] = $name;
            }
            if (empty($record['message'])) {
                $record['message'] = 'UNKNOWN_SHORT_MESSAGE';
            }

            return $record;
        });
    }

    /**
     * @return MonologLogger
     */
    private function getMonolog(): MonologLogger
    {
        /** @var LogManager $logManager */
        $logManager = $this->app['log'];
        /** @var Logger $driver */
        $driver = $logManager->driver();
        /** @var MonologLogger $logger */
        $logger = $driver->getLogger();

        return $logger;
    }
}
