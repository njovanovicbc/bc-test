<?php

namespace App\Providers;

use App\Repositories\Interfaces\PredictionRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\PredictionRepository;
use App\Repositories\UserRepository;
use App\Services\Interfaces\TokenInterface;
use App\Services\JWTTokenService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(PredictionRepositoryInterface::class, PredictionRepository::class);
        $this->app->bind(TokenInterface::class, JWTTokenService::class);
    }
}
