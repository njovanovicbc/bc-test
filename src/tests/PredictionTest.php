<?php
declare(strict_types=1);

namespace Tests;

use App\Exceptions\NotFoundException;
use App\Factories\PredictionFactory;
use App\Http\Dto\Request\PredictionDto;
use App\Models\Prediction;
use App\Repositories\Interfaces\PredictionRepositoryInterface;
use App\Services\PredictionService;
use Illuminate\Database\Eloquent\Collection;
use PHPUnit\Framework\MockObject\MockObject;

class PredictionTest extends TestCase
{
    /** @var PredictionService */
    private $predictionService;

    /** @var MockObject|PredictionRepositoryInterface */
    private $predictionRepositoryMock;

    /** @var MockObject|PredictionFactory */
    private $predictionFactory;

    protected function setUp(): void
    {
        parent::setUp();

        $this->predictionRepositoryMock = $this->getMockBuilder(PredictionRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->predictionFactory = new PredictionFactory();

        $this->predictionService = new PredictionService($this->predictionRepositoryMock, $this->predictionFactory);
    }

    /**
     * Tests PredictionService::getAllPredictions
     */
    public function testGetAllPredictions(): void
    {
        $collection = new Collection();
        $collection->add($this->preparePrediction(1, Prediction::TYPE_CORRECT_RESULT, '3:2'));
        $collection->add($this->preparePrediction(2, Prediction::TYPE_3_WAY, 'X'));

        $this->predictionRepositoryMock->expects($this->once())
            ->method('getAllPredictions')
            ->willReturn($collection);

        $predictions = $this->predictionService->getAllPredictions();

        self::assertCount(2, $predictions);
    }

    /**
     * Tests PredictionService::createPrediction
     */
    public function testCreatePrediction(): void
    {
        $predictionDto = new PredictionDto([
            'event_id' => 1,
            'market_type' => Prediction::TYPE_3_WAY,
            'prediction' => 'X'
        ]);

        $prediction = $this->predictionService->createPrediction($predictionDto);

        self::assertSame($predictionDto->event_id, $prediction->getEventId());
        self::assertSame($predictionDto->market_type, $prediction->getMarketType());
        self::assertSame($predictionDto->prediction, $prediction->getPrediction());
    }

    /**
     * Tests PredictionService::updatePredictionStatus
     * @throws NotFoundException
     */
    public function testUpdatePredictionStatusFailed(): void
    {
        $this->predictionRepositoryMock->expects($this->once())
            ->method('getPrediction')
            ->willReturn(null);
        $this->expectException(NotFoundException::class);
        $this->predictionService->updatePredictionStatus(1, Prediction::STATUS_WIN);
    }

    /**
     * Tests PredictionService::updatePredictionStatus
     * @throws NotFoundException
     */
    public function testUpdatePredictionStatusSuccess(): void
    {
        $this->predictionRepositoryMock->expects($this->once())
            ->method('getPrediction')
            ->willReturn($this->preparePrediction(1, Prediction::TYPE_CORRECT_RESULT, '3:2'));
        $prediction = $this->predictionService->updatePredictionStatus(1, Prediction::STATUS_WIN);

        self::assertSame(Prediction::STATUS_WIN, $prediction->getStatus());
    }

    /**
     * @param int $eventId
     * @param string $marketType
     * @param string $predictionValue
     * @return Prediction
     */
    private function preparePrediction(int $eventId, string $marketType, string $predictionValue): Prediction
    {
        $prediction = new Prediction();

        $prediction
            ->setEventId($eventId)
            ->setMarketType($marketType)
            ->setPrediction($predictionValue);

        return $prediction;
    }
}
