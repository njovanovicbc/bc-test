<?php
declare(strict_types=1);

namespace Tests;

use App\Exceptions\InvalidParameterValueException;
use App\Http\Dto\Request\PredictionDto;
use App\Http\Validators\PredictionsValidator;
use App\Models\Prediction;

class PredictionValidatorTest extends TestCase
{
    /** @var PredictionsValidator */
    private $predictionsValidator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->predictionsValidator = new PredictionsValidator();
    }

    /**
     * Tests PredictionsValidator::validateCorrectValuesAreSet
     * @throws InvalidParameterValueException
     */
    public function testValidateCorrectValuesAreSetSuccess(): void
    {
        $predictionDtoV1 = new PredictionDto([
            'event_id' => 1,
            'market_type' => Prediction::TYPE_CORRECT_RESULT,
            'prediction' => '1:2'
        ]);
        $predictionDtoV2 = new PredictionDto([
            'event_id' => 1,
            'market_type' => Prediction::TYPE_3_WAY,
            'prediction' => 'X'
        ]);
        $this->predictionsValidator->validateCorrectValuesAreSet($predictionDtoV1);
        $this->predictionsValidator->validateCorrectValuesAreSet($predictionDtoV2);
        $this->expectNotToPerformAssertions();
    }

    /**
     * Tests PredictionsValidator::validateCorrectValuesAreSet
     * @throws InvalidParameterValueException
     */
    public function testValidateCorrectValuesAreSetFailedBadType(): void
    {
        $predictionDtoBadType = new PredictionDto([
            'event_id' => 1,
            'market_type' => 'invalid_market_type',
            'prediction' => 'X'
        ]);
        $this->expectException(InvalidParameterValueException::class);
        $this->expectExceptionMessage('INVALID_MARKET_TYPE, Must be one of: ' . implode(', ', Prediction::getValidTypes()));
        $this->predictionsValidator->validateCorrectValuesAreSet($predictionDtoBadType);
    }

    /**
     * Tests PredictionsValidator::validateCorrectValuesAreSet
     * @throws InvalidParameterValueException
     */
    public function testValidateCorrectValuesAreSetFailedBadResultFormat(): void
    {
        $predictionDtoBadResultFormat = new PredictionDto([
            'event_id' => 1,
            'market_type' => Prediction::TYPE_CORRECT_RESULT,
            'prediction' => '1-2'
        ]);
        $this->expectException(InvalidParameterValueException::class);
        $this->expectExceptionMessage('Invalid prediction format, must be score divided by :, for example 0:0, 1:0, 5:3');
        $this->predictionsValidator->validateCorrectValuesAreSet($predictionDtoBadResultFormat);
    }

    /**
     * Tests PredictionsValidator::validateCorrectValuesAreSet
     * @throws InvalidParameterValueException
     */
    public function testValidateCorrectValuesAreSetFailedBad3Way(): void
    {
        $predictionDtoBad3Way = new PredictionDto([
            'event_id' => 1,
            'market_type' => Prediction::TYPE_3_WAY,
            'prediction' => 'y'
        ]);
        $this->expectException(InvalidParameterValueException::class);
        $this->expectExceptionMessage('Prediction must be one of: ' . implode(', ', Prediction::VALID_3_WAY_PREDICTIONS));
        $this->predictionsValidator->validateCorrectValuesAreSet($predictionDtoBad3Way);
    }
}
