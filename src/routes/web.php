<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Middleware\SwaggerDocsMiddleware;

$router->group(['prefix' => 'v1'], function () use ($router) {

    $router->post('login', 'AuthController@login');

    $router->post('users', 'UsersController@create');

    $router->group(['middleware' => ['auth']], static function () use ($router) {
        $router->group([
            'prefix' => 'predictions',
        ], static function () use ($router) {
            $router->get('/', 'PredictionsController@getAll');
            $router->post('/', 'PredictionsController@create');
            $router->put('/{id}/status', 'PredictionsController@updateStatus');
        });
    });
});

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['middleware' => [SwaggerDocsMiddleware::class]], static function () use ($router) {
    $router->get('/api-docs', [
        'as' => 'swagger-lume.docs',
        'uses' => 'Swagger\SwaggerController@docs',
    ]);

    $router->get('/docs', [
        'as' => 'swagger-lume.api',
        'uses' => 'Swagger\SwaggerController@api',
    ]);

    $router->get('/swagger-ui-assets/{asset}', [
        'as' => 'swagger-lume.asset',
        'uses' => 'Swagger\SwaggerController@getAsset',
    ]);

    $router->get('/api/oauth2-callback', [
        'as' => 'swagger-lume.oauth2_callback',
        'uses' => 'Swagger\SwaggerController@oauth2Callback',
    ]);
});
