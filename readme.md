# Laravel/Lumen Docker Scaffold

### **Description**

This will create a dockerized stack for a Laravel/Lumen application, consisted of the following containers:
-  **app**, your PHP application container

        Nginx, PHP7.4 PHP7.4-fpm, Composer
    
-  **mysql**, MySQL database container ([mysql](https://hub.docker.com/_/mysql/) official Docker image)

#### **Directory Structure**
```
+-- src <project root> => backend api
+-- resources
|   +-- default
|   +-- nginx.conf
|   +-- supervisord.conf
|   +-- www.conf
+-- .gitignore
+-- Dockerfile
+-- docker-compose.yml
+-- readme.md <this file>
```

### **Setup instructions**

**Prerequisites:** 

* Depending on your OS, the appropriate version of Docker Community Edition has to be installed on your machine.  ([Download Docker Community Edition](https://hub.docker.com/search/?type=edition&offering=community))

**Installation steps:** 

1. Create a new directory in which your OS user has full read/write access and clone this repository inside.

2. Open a new terminal/CMD, navigate to this repository root (where `docker-compose.yml` exists) and execute the following command:

    ```
    $ docker-compose up -d
    ```

    This will download/build all the required images and start the stack containers. It usually takes a bit of time, so grab a cup of coffee.

3. To start up the service.
    ```
    $ docker exec -it bc_nikola bash
    $ composer install
    $ php artisan migrate
    ```
   MySql user, database and credentials are already set, and they are all the same "bc", mySql port is exposed on the port 33063, while nginx is running on port 85.

4. Navigate to [http://localhost:85](http://localhost:85) to access the application.

It is possible that permissions against storage folder should be corrected for logs and test results to be generated properly.

6. For running unit tests with coverage
   ```
   $ XDEBUG_MODE=coverage ./vendor/bin/phpunit
   ```
   Results available in storage/tests/codeCoverage/index.html

7. For running static analyzer
   ```
   $ vendor/bin/phpstan analyse -c phpstan.neon
   ```
   
8. Postman collection available on this path
   ```
   src/Nikola BC collection.postman_collection.json
   ```
9. To generate swagger documentation run
   ```
   $ php artisan swagger-lume:generate
   ```
   and docs can be browsed here [http://localhost:85/docs](http://localhost:85/docs). It's possible to run all endpoints from swagger UI.
10. To test the app first create some test user using this endpoint POST http://localhost:85/v1/users/register. 
Then use those credentials to login.
All other endpoints will require Authorization token to be accessed.
If you are using postman collection, Login endpoint will automatically store the token in the environment and will be automatically added for all other endpoints.
